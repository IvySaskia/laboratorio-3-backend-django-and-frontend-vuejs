import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import ListSong from '@/components/Song/ListSong'
import EditSong from '@/components/Song/EditSong'
import DeleteSong from '@/components/Song/DeleteSong'
import NewSong from '@/components/Song/NewSong'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ListSong',
      component: ListSong
    },
    {
      path: '/songs',
      name: 'ListSong',
      component: ListSong    	
    },
    {
      path: '/songs/:songId/edit',
      name: 'EditSong',
      component: EditSong    	
    },
    {
      path: '/songs/:songId/delete',
      name: 'DeleteSong',
      component: DeleteSong    	
    },
    {
      path: '/songs/new',
      name: 'NewSong',
      component: NewSong    	
    }
     
  ],
  mode:'history'
})
