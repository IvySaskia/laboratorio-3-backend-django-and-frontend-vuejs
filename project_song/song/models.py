from django.db import models

# Create your models here.

class Song(models.Model):
	title = models.CharField(max_length=30)
	lyrics = models.TextField()
	artist = models.CharField(max_length=50)
	gender = models.CharField(max_length=10)
