from rest_framework import routers

from .viewsets import SongViewSet

router = routers.SimpleRouter()
router.register('songs',SongViewSet)

urlpatterns = router.urls
